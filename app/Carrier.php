<?php

namespace App;

use App\Interfaces\CarrierInterface;
use App\Services\ContactService;
use App\Call;

class Carrier implements CarrierInterface
{
    protected $contact;

    public function dialContact(Contact $contact)
    {
        $this->contact = $contact;
    }

    public function makeCall(): Call
    {
        return new Call();
    }

}
