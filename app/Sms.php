<?php

namespace App;
use App\Services\ContactService;
/**
 * Sms
 */
class Sms
{
    public function send($number , $body)
    {
        if (!ContactService::validateNumber($number)) {
            return;
        }
        return $this->make($number,$body);
    }
    public function make($number,$body){
        return 'sent!';
    }
}