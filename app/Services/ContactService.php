<?php

namespace App\Services;

use App\Contact;


class ContactService
{
	
	public static function findByName($name)//: Contact
	{
		// queries to the db
        $contact = new Contact();

        if ($name == $contact->getName() ) {
            return $contact;
        }
        return false;
	}

	public static function validateNumber(string $number): bool
	{
		if( is_numeric( $number ) && strlen($number)==9 ) {
            return true;
        }
        return false;
	}
}