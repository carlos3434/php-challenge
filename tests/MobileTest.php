<?php

namespace Tests;

use Mockery as m;
use PHPUnit\Framework\TestCase;
use App\Mobile; 
use App\Carrier;
use App\Sms;

class MobileTest extends TestCase
{
	
	/**
     * @test
     * @dataProvider providerCarrier
      */
	public function it_returns_null_when_name_empty(Carrier $provider)
	{
		$mobile = new Mobile($provider);

		$this->assertNull($mobile->makeCallByName(''));
	}

    /**
     * @test
     * @dataProvider providerUserName
      */
    public function it_returns_an_user( Carrier $provider , $name )
    {
        $mobile = new Mobile($provider);

        $this->assertIsObject($mobile->makeCallByName( $name ));
    }
    /**
     * @test
     * @dataProvider providerUserNameNotFound
      */
    public function when_user_not_found( Carrier $provider , $name )
    {
        $mobile = new Mobile($provider);
        $this->assertEquals( 'not found', $mobile->makeCallByName( $name ) );
    }

    /**
     * @test
     * @dataProvider providerContact
      */
    public function send_sms( $number , $body )
    {

        $sms= new Sms();
        $this->assertEquals( 'sent!', $sms->send( $number, $body ) );
    }

    /**
     * @test
      */
    public function not_send_sms(   )
    {

        $sms= new Sms();
        $this->assertNull(   $sms->send( '99999999a', 'hey there' ) );
    }
    

    public function providerCarrier()
    {
        return [
            [new Carrier()]
        ];
    }
    public function providerUserName()
    {
        return [
            [new Carrier(), 'juan']
        ];
    }
    public function providerUserNameNotFound()
    {
        return [
            [new Carrier(), 'jose']
        ];
    }
    public function providerContact()
    {
        return [
            ['999999999', 'hey there'],
        ];
    }
}
